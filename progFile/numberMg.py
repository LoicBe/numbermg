

suffixe = {
    "0" : "",
    "1" : "iray" ,
    "2" : "roa" ,
    "3" : "telo" ,
    "4" : "efatra" ,
    "5" : "dimy" ,
    "6" : "enina" ,
    "7" : "fito" ,
    "8" : "valo" ,
    "9" : "sivy" ,
    "10" : "folo" ,
    "20" : "roapolo" ,
    "30" : "telopolo" ,
    "40" : "efapolo" ,
    "50" : "dimapolo" ,
    "60" : "enimpolo" ,
    "70" : "fitopolo" ,
    "80" : "valopolo" ,
    "90" : "sivy folo",
    "100" : "zato",
    "200" : "roanjato" ,
    "300" : "telonjato" ,
    "400" : "efajato" ,
    "500" : "dimanjato" ,
    "600" : "eninjato" ,
    "700" : "fitonjato" ,
    "800" : "valonjato" ,
    "900" : "sivinjato" , 
    "1000" : "arivo",
    "10000" : "alina"
}


number_details = {
    "1" : None ,
    "10" : None ,
    "100" : None ,
    "1000" : None ,
    "10000" :  None ,
    "100000" : None , 
    "1000000" : None ,
    "10000000" : None
}

number_suffixe = {
    "1" : "" ,
    "10" : " ambi -" ,
    "100" : " sy " ,
    "1000" : " sy "
}



def get_suffixe(key , val ):
    return suffixe[str(val*int(key))]




def get_suffixeByVal(key ,numberVal):
    if key == "10" and numberVal == 0:
        return "  "
    if key == "100" and numberVal==1 :
        return " amby "
    else:
        return number_suffixe[key]
    
    

def printDetails():
    for k ,  v in number_details.items():
        print ( k , v)



def extract_number_details( number_details ,number):
    for k , v in number_details.items():
        print(k ,  v)
        if (len(str(number)) >= len(k)) :
            number_details[k] = (number//int(k)) % 10
    return number_details



def translate(number) : 
    strNumber = ''
    details = extract_number_details(number_details , number)
    printDetails()
    for k , v in details.items() :
        if v != None :
            print (k , v ,suffixe[str(v*int(k))] ,number_suffixe[k])
            strNumber = strNumber + get_suffixeByVal(k,v) + get_suffixe(k,v)
            

    return strNumber



